# ng-multimedia-capture

This creates 3 common angular components to capture media built around the standard media APIs.

MediaRecorder is not yet shipped in all browsers, but the MediaStreams API seems quite supported in all major browsers, so at least we can access the camera.

## TODO

- code documentation
- tests
- better error handling

# Usage

here is what is in our demo html files, for more info please go view our demo component, or please view our [wiki](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/wikis/Component-Documentation).

### video
```
<ng-media-video #video (waitingForMedia)="print($event)" (notSupported)="print($event)" (onStarted)="print($event)" (onCapture)="print($event)" (onFinished)="setVideo($event, video)"></ng-media-video>

<button (click)="video.start()" >start</button>
<button (click)="video.capture()">capture</button>
<button (click)="video.stop()">stop</button>
<button (click)="video.reset()">reset</button>
```
### snapshot
```
<ng-media-snapshot #picture (waitingForMedia)="print($event)" (notSupported)="print($event)" (onStarted)="print($event)" (onCapture)="print($event)"></ng-media-snapshot>

<button (click)="picture.start()" >start</button>
<button (click)="picture.capture()">capture</button>
<button (click)="picture.reset()">reset</button>
```
### audio
```
<ng-media-audio #audio (waitingForMedia)="print($event)" (notSupported)="print($event)" (onStarted)="print($event)" (onCapture)="print($event)" (onFinished)="setAudio($event, audio)"></ng-media-audio>

<button (click)="audio.start()" >start</button>
<button (click)="audio.capture()">capture</button>
<button (click)="audio.stop()">stop</button>
<button (click)="audio.reset()">reset</button>
```

# Committing

Please follow [Conventional Commit](https://www.conventionalcommits.org) specifications. This will be the standard for all repositories. Please update this readme with further documentation with major changes/features everyone should know about.

**tools available**

-   [Commitizen](http://commitizen.github.io/cz-cli/)

## Development server

Run `npm start`

Navigate to `http://localhost:4201/`.

The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Releases

we're using standard-version to tag our releases.

make sure you have everything committed, and all tests are passing. Make sure its not a Friday as well.

you can do `standard-version --dryrun` to see what will happen

then/or run `standard-version` to tag a release, and push it.

`git push --follow-tags origin master`

### npm

<https://www.npmjs.com/package/@momento/ng-multimedia-capture>

#### publishing to npm

We build our npm package with `ng-packagr`. 

to build run `npm run build:npm`, it will then build to `./dist`.

`cd  dist` and you can publish with `npm publish --access public` if you have the right access.

## useful links

<https://www.npmjs.com/package/@types/dom-mediacapture-record>
<https://w3c.github.io/mediacapture-record/MediaRecorder.htm>
<https://developer.mozilla.org/en-US/docs/Web/API/MediaStream>
<https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices>  
<https://developers.google.com/web/fundamentals/>
<https://developers.google.com/web/fundamentals/media/video>  
<https://caniuse.com/#feat=mediarecorder>
