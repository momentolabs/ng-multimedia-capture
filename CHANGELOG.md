# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.4](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/compare/v0.1.3...v0.1.4) (2019-07-01)


### Bug Fixes

* **snapshot:** onCapture emitter emits blob instead of null ([aa9710e](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/aa9710e))



## [0.1.3](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/compare/v0.1.1...v0.1.3) (2019-07-01)


### Bug Fixes

* **snapshot:** stop camera on component destroy ([03b7518](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/03b7518))
* **snapshot:** styling & make camera/view getters public ([e0afff6](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/e0afff6))



## [0.1.2](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/compare/v0.1.1...v0.1.2) (2019-07-01)


### Bug Fixes

* **snapshot:** styling & make camera/view getters public ([e0afff6](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/e0afff6))



## [0.1.1](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/compare/v0.1.0...v0.1.1) (2019-06-26)


### Bug Fixes

* make mediaRecorder a public member for better interaction ([d9ff129](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/d9ff129))
* stop stream when component is destroyed ([5cdf2306](https://gitlab.com/momentolabs/ng-multimedia-capture/commit/5cdf2306e0435de13f44c29e031ee3440b093408))


<a name="0.1.0"></a>
# 0.1.0 (2019-04-21)


### Bug Fixes

* update demo to latest changes, reuse code & change dev server port to be 4021 ([934a349](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/934a349))
* **dependencies:** fixed issues with our dependency structure ([acf9612](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/acf9612))
* **media:** further hardening of media components ([5cce771](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/5cce771))
* **media recorder:** use defintlyTyped def for media recorder and remove useless polyfil. ([c7a31e5](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/c7a31e5))
* **snapshot:** init onInit event ([920b140](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/920b140))
* **tsconfig.app:** remove empty typeRoot and types from tsconfig.app ([418092a](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/418092a))
* **video:** fix condition of supported ([eec9d82](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/eec9d82))
* **video:** fixed video container not muting ([612ffdd](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/612ffdd))
* **video:** reset srcObject ([e13a6af](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/e13a6af))


### Features

* **audio:** added audio capabilites ([ca83004](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/ca83004))
* **demo:** add demo component for testing ([1c9e492](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/1c9e492))
* **media/common & video:** add MediaNotSupportedException & handled it in video component ([ba7412a](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/ba7412a))
* **MediaComponent:** add mediaConstrainsts and make sure it's spelled right ([9b107f3](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/9b107f3))
* **typescript:** added custom types directory ([0f53bb0](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/0f53bb0))
* **video:** intial video capture ([8c57cff](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/8c57cff))
* **video:** saves video to webm after recording has ended ([225a8a3](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/225a8a3))
* **video & audio:** add reset function to reset media source ([abf3a42](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/abf3a42))
* add multi media interface class to help keep components coherent ([c86eefb](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/c86eefb))
* **Snapshot:** added snapshot capabilties ([24f8574](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/24f8574))
* **video:** finishedRecording now emits a Blog object instead of MediaStream ([1ff2079](https://gitlab.com/momentolabs/mmnto-ng-multimedia-capture/commit/1ff2079))
