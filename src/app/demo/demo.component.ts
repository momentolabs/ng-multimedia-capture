import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.less']
})
export class DemoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  print(v: any) {
    console.log(v);
  }

  setAudio(event, mic) {
    this.print(event);
    mic.setAudioSrc(event);
  }

  setVideo(event, vid) {
    this.print(event);
    vid.setVideoSrc(event);
  }


}
