import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MediaModule } from './media/media.module';
import { RouterModule, Routes } from '@angular/router'


const routes: Routes = [
  {
    path: 'demo',
    loadChildren: './demo/demo.module#DemoModule'
  },
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    MediaModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
