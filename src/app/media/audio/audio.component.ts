import { Component, ElementRef, EventEmitter, Output, OnInit } from '@angular/core';
import { MediaNotSupportedException } from '../common/media-not-supported.exception';
import { MediaComponent } from '../media.interface';
import { MediaRecorderAlreadyActive } from '../common/media-recorder-already-active.exception';
import { MediaStreamNotFoundException } from '../common/media-stream-not-found.exception';

@Component({
  selector: 'ng-media-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.less']
})
export class AudioComponent implements MediaComponent, OnInit {


  @Output() onInit: EventEmitter<any>;
  @Output() notSupported: EventEmitter<any>;
  @Output() waitingForMedia: EventEmitter<boolean>;
  @Output() onStarted: EventEmitter<any>;
  @Output() onCapture: EventEmitter<any>;
  @Output() onFinished: EventEmitter<any>;

  mediaRecorder: MediaRecorder;

  mediaConstraints: MediaStreamConstraints = { audio: true, video: false };

  DEFAULT_CAPTURE_ENCODING = { mimeType: 'audio/webm' };

  constructor(private elementRef: ElementRef) {
    this.notSupported = new EventEmitter<any>();
    this.waitingForMedia = new EventEmitter<boolean>();
    this.onStarted = new EventEmitter<any>();
    this.onCapture = new EventEmitter<any>();
    this.onFinished = new EventEmitter<any>();
    this.onInit = new EventEmitter<any>();

  }

  ngOnInit() {
    this.onInit.emit();
  }

  async start(): Promise<void> {
    let supported: boolean = 'mediaDevices' in navigator;
    if (!supported) {
      this.notSupported.emit();
      throw new MediaNotSupportedException();
    }

    // reset view before starting
    this.reset();
    // check media recorder state
    if (this.mediaRecorder != null && this.mediaRecorder.state != 'inactive') {
      throw new MediaRecorderAlreadyActive();
    }
    this.setMuted(true);

    this.waitingForMedia.emit(true);
    try {
      const stream = await navigator.mediaDevices.getUserMedia(this.mediaConstraints);
      return this.handleMedia(stream);
    } catch (error) {
      return this.handleErrors(error);
    }
  }

  capture(options?: any): void {
    if (this.microphone.srcObject == null) {
      throw new MediaStreamNotFoundException();
    }

    let stream = this.microphone.srcObject;
    options = options ? options : this.DEFAULT_CAPTURE_ENCODING;

    this.mediaRecorder = new MediaRecorder(stream, options);
    this.mediaRecorder.start();
    this.mediaRecorder.ondataavailable = (blob) => {
      this.onFinished.emit(blob.data);
    };
  }

  stop() {
    if (this.microphone.srcObject != null) {
      let tracks = this.microphone.srcObject.getTracks();
      tracks.forEach((track: MediaStreamTrack) => {
        track.stop();
      });
      this.microphone.srcObject = null;
    }
    if (this.mediaRecorder != null) this.mediaRecorder.stop();
    this.onFinished.emit();
  }

  handleMedia(stream: MediaStream) {
    this.microphone.srcObject = stream;
    this.onStarted.emit();
    this.waitingForMedia.emit(false);
  }

  handleErrors(error: DOMException) {
    // TODO: handle errors
    // https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia#Exceptions
    console.log(error);
  }

  setAudioSrc(blob) {
    this.setMuted(false);
    this.microphone.src = URL.createObjectURL(blob);
  }

  reset() {
    this.microphone.srcObject = null;
    this.microphone.src = null;
  }

  setMuted(muted: boolean) {
    // https://stackoverflow.com/questions/14111917/html5-video-muted-but-still-playing
    this.microphone.muted = muted;
  }

  get isMuted() {
    return this.microphone.muted;
  }

  private get microphone() {
    return this.elementRef.nativeElement.querySelector("#microphone")
  }


}
