export class MediaRecorderAlreadyActive extends Error {
  constructor() {
    super("MediaRecorder already active");
    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
  }
}