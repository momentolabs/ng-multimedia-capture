export class MediaNotSupportedException extends Error {
  constructor() {
    super("getUserMedia not supported");
    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
  }
}