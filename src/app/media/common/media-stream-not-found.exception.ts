export class MediaStreamNotFoundException extends Error {
  constructor() {
    super("No media stream found");
    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
  }
}