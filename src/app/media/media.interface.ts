import { EventEmitter } from '@angular/core';

export interface MediaComponent {
  start(): Promise<any>;
  capture(options?: any): void;
  stop(): void;
  reset(): void;
  mediaRecorder: any;
  mediaConstraints: MediaStreamConstraints
  onInit: EventEmitter<any>
  notSupported: EventEmitter<any>;
  waitingForMedia: EventEmitter<boolean>;
  onStarted: EventEmitter<any>;
  onCapture: EventEmitter<any>;
  onFinished: EventEmitter<any>;
}
