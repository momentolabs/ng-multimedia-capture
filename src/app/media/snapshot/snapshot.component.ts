import { Component, OnInit, Output, EventEmitter, ElementRef, Input } from '@angular/core';
import { MediaNotSupportedException } from '../common/media-not-supported.exception';
import { MediaComponent } from '../media.interface';

@Component({
  selector: 'ng-media-snapshot',
  templateUrl: './snapshot.component.html',
  styleUrls: ['./snapshot.component.less']
})
export class SnapShotComponent implements OnInit, MediaComponent {


  @Input() viewHeight = 1080;
  @Input() viewWidth = 1920;

  @Output() onInit: EventEmitter<null>;
  @Output() notSupported: EventEmitter<any>;
  @Output() waitingForMedia: EventEmitter<boolean>;
  @Output() onStarted: EventEmitter<null>;
  @Output() onCapture: EventEmitter<Blob>;
  @Output() onFinished: EventEmitter<null>;

  mediaRecorder = null;

  mediaConstraints: MediaStreamConstraints = { video: true, audio: false };

  constructor(private elementRef: ElementRef) {
    this.notSupported = new EventEmitter<any>();
    this.waitingForMedia = new EventEmitter<boolean>();
    this.onStarted = new EventEmitter<any>();
    this.onCapture = new EventEmitter<any>();
    this.onInit = new EventEmitter<any>();
  }

  ngOnInit() {
    this.camera.muted = true;
    this.setViewVisable(false);
    this.onInit.emit();
  }

  // lets the user start using the camera to take a snapshot
  public async start(): Promise<void> {
    let supported: boolean = 'mediaDevices' in navigator;
    if (!supported) {
      this.notSupported.emit();
      throw new MediaNotSupportedException();
    }
    // reset view before recording
    this.stop();

    this.waitingForMedia.emit(true);
    try {
      const stream = await navigator.mediaDevices.getUserMedia(this.mediaConstraints);
      return this.handleMedia(stream);
    } catch (error) {
      this.handleErrors(error);
    }
  }

  // captures the current view from the camera, and draws it to the canvas
  public capture() {
    this.viewContext.drawImage(this.camera, 0, 0, this.view.width, this.view.height);
    this.view.toBlob((blob: Blob) => {
      this.onCapture.emit(blob);
    });
    this.resetCamera();
    this.setCameraVisible(false);
    this.setViewVisable(true);
  }

  public stop(): void {
    this.reset();
  }

  private handleMedia(stream: MediaStream) {
    this.camera.srcObject = stream;
    this.waitingForMedia.emit(false);
    this.onStarted.emit();
  }

  private handleErrors(error: DOMException) {
    // TODO: handle errors
    // https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia#Exceptions
    this.waitingForMedia.emit(false);
    console.log(error);
  }

  public reset(): void {
    this.resetCamera();
    this.resetImageView();
  }

  public resetCamera() {
    if (this.camera.srcObject != null) {
      let tracks = this.camera.srcObject.getTracks();
      tracks.forEach((track: MediaStreamTrack) => {
        track.stop();
      });
      this.camera.srcObject = null;
    }
    this.setCameraVisible(true);
  }

  public resetImageView() {
    this.viewContext.clearRect(0, 0, this.view.width, this.view.height)
    this.setViewVisable(false);
  }

  get camera() {
    return this.elementRef.nativeElement.querySelector('#camera');
  }

  get view() {
    return this.elementRef.nativeElement.querySelector('#view');
  }

  private get viewContext() {
    return this.view.getContext('2d');
  }

  private setViewVisable(show: boolean) {
    this.view.style.display = show ? 'block' : 'none';
  }

  private setCameraVisible(show: boolean) {
    this.camera.style.display = show ? 'block' : 'none';
  }

  ngOnDestroy() {
    this.stop();
  }
}
