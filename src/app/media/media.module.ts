import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoComponent } from './video/video.component';
import { AudioComponent } from './audio/audio.component';
import { SnapShotComponent } from './snapshot/snapshot.component';


@NgModule({
  declarations: [
    VideoComponent,
    AudioComponent,
    SnapShotComponent
  ],
  exports: [
    VideoComponent,
    SnapShotComponent,
    AudioComponent
  ],
  imports: [
    CommonModule
  ],
})
export class MediaModule { }
