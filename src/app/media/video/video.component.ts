import { Component, EventEmitter, Output, ElementRef, OnInit } from '@angular/core';
import { MediaNotSupportedException } from '../common/media-not-supported.exception';
import { MediaComponent } from '../media.interface';
import { MediaStreamNotFoundException } from '../common/media-stream-not-found.exception';
import { MediaRecorderAlreadyActive } from '../common/media-recorder-already-active.exception';

@Component({
  selector: 'ng-media-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.less']
})
export class VideoComponent implements MediaComponent, OnInit {

  DEFAULT_CAPTURE_ENCODING = { mimeType: 'video/webm' };

  @Output() onInit: EventEmitter<any>;
  @Output() notSupported: EventEmitter<any>;
  @Output() waitingForMedia: EventEmitter<boolean>;
  @Output() onStarted: EventEmitter<any>;
  @Output() onCapture: EventEmitter<any>;
  @Output() onFinished: EventEmitter<any>;

  mediaRecorder: MediaRecorder;
  mediaConstraints: MediaStreamConstraints = { audio: true, video: true };

  constructor(private elementRef: ElementRef) {
    this.notSupported = new EventEmitter<any>();
    this.waitingForMedia = new EventEmitter<boolean>();
    this.onStarted = new EventEmitter<any>();
    this.onCapture = new EventEmitter<any>();
    this.onFinished = new EventEmitter<any>();
    this.onInit = new EventEmitter<any>();
  }

  ngOnInit() {
    this.onInit.emit();
    console.log('Hello, we have inited with stream close');
  }

  public async start(): Promise<void> {
    this.waitingForMedia.emit(true);

    let supported: boolean = 'mediaDevices' in navigator;
    if (!supported) {
      this.notSupported.emit();
      throw new MediaNotSupportedException();
    }

    // reset camera before recording
    this.reset();
    // check media recorder state
    if (this.mediaRecorder != null && this.mediaRecorder.state != 'inactive') {
      throw new MediaRecorderAlreadyActive();
    }
    this.setMuted(true);

    try {
      const stream = await navigator.mediaDevices.getUserMedia(this.mediaConstraints);
      return this.handleMedia(stream);
    }
    catch (error) {
      return this.handleErrors(error);
    }
  }

  public capture(options?: any) {

    if (this.camera.srcObject == null) {
      throw new MediaStreamNotFoundException();
    }

    let stream = this.camera.srcObject;
    options = options ? options : this.DEFAULT_CAPTURE_ENCODING;

    this.mediaRecorder = new MediaRecorder(stream, options);
    this.mediaRecorder.start();
    this.onCapture.emit();
    this.mediaRecorder.ondataavailable = (blob) => {
      this.onFinished.emit(blob.data);
    };
  }

  public stop() {
    if (this.camera.srcObject != null) {
      let tracks = this.camera.srcObject.getTracks();
      tracks.forEach((track: MediaStreamTrack) => {
        track.stop();
      });
      this.camera.srcObject = null;
    }
    if (this.mediaRecorder != null && this.mediaRecorder.state != 'inactive') this.mediaRecorder.stop();
  }

  public reset() {
    this.camera.srcObject = null;
    this.camera.src = null;
  }

  public setMuted(muted: boolean) {
    // https://stackoverflow.com/questions/14111917/html5-video-muted-but-still-playing
    this.elementRef.nativeElement.querySelector('#camera').muted = muted;
  }

  public get isMuted(): boolean {
    return this.elementRef.nativeElement.querySelector('#camera').muted;
  }

  private handleMedia(stream: MediaStream) {
    this.camera.srcObject = stream;
    this.waitingForMedia.emit(false);
    this.onStarted.emit();
  }

  private handleErrors(error: DOMException) {
    // TODO: handle errors
    // https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia#Exceptions
    this.waitingForMedia.emit(false);
    console.log(error);
  }

  public setVideoSrc(blob: Blob) {
    this.setMuted(false);
    this.camera.src = URL.createObjectURL(blob);
  }

  private get camera() {
    return this.elementRef.nativeElement.querySelector('#camera');
  }

  ngOnDestroy() {
    this.stop();
  }

}
